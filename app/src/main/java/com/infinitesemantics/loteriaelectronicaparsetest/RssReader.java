package com.infinitesemantics.loteriaelectronicaparsetest;


import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/*
   RssReader: this class reads through the URL provided in MainActivity.java (declared as private
              here) and parses it into a List abstract data type. Uses the Java SAX (Simple API
              for XML) parser.

   Note 1:    This file COULD be directly copied into the code. The code here SHOULD "just work" as
              intended in every usage case. If not, elbow grease it is!
*/

public class RssReader {
    private String rssUrl;

    public RssReader(String url) {
        rssUrl = url;
    }

    public List<RssItem> getItems() throws Exception {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        // Creates a new RssHandler, which will do the parsing.
        RssHandler handler = new RssHandler();
        // Passes the RssHandler that was created to the SaxParser.
        saxParser.parse(rssUrl, handler);
        return handler.getRssItemList();
    }
}