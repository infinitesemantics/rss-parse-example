package com.infinitesemantics.loteriaelectronicaparsetest;

/*
    RssItem: this class is an interface that specifies items to be read from an XML document.

    Note 1: Can and should be modified to accommodate the tags that are needed from the document.
            Each needs to be declared as a String resource, and have a getter and a setter.

    Note 2: This one specifically has a few more added items to serve as examples. They don't
            all work for the sample Lotería Electrónica URL I have in MainActivity.java!

    Note 3: This file SHOULDN'T be directly copied into the code. Just create a new .java into
            the app's directory with the same name and copy the code you think you'd need.
*/

public class RssItem {
    String title;
    String description;
    String link;
    String imageUrl;
    String date;

    /* ---------- GETTERS ---------- */
    public String getDescription() {
        return description;
    }

    /* ---------- SETTERS ---------- */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}