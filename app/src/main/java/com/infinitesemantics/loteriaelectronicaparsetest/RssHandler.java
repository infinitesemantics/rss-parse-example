package com.infinitesemantics.loteriaelectronicaparsetest;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/*
    RssHandler:

    Note 1:

    Note 2: This one specifically has a few more added items to serve as examples. They don't
            all work for the sample Lotería Electrónica URL I have in MainActivity.java!

    Note 3: This file SHOULDN'T be directly copied into the code. Just create a new .java into
            the app's directory with the same name and copy the code you think you'd need.
*/


public class RssHandler extends DefaultHandler {
    private List<RssItem> rssItemList;
    private RssItem currentItem;
    private boolean parsingTitle;
    private boolean parsingLink;
    private boolean parsingDescription;
    private boolean parsingDate;

    // Initializes a new ArrayList that will hold all the generated RSS items.
    public RssHandler() {
        rssItemList = new ArrayList<RssItem>();
    }

    // Returns the RSS items list when it is called.
    public List<RssItem> getRssItemList() {
        return rssItemList;
    }

    /*
        Called when an opening tag is reached, such as <item> or <title>. This should be modified
        to add or remove depending on the use it will receive (i.e. if what you want is inside
        a <title> tag, <description> tag, <description> tag, etc.)
    */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("item"))
            currentItem = new RssItem();
        else if (qName.equals("title"))
            parsingTitle = true;
        else if (qName.equals("link"))
            parsingLink = true;
        else if (qName.equals("description"))
            parsingDescription = true;
        else if (qName.equals("pubDate"))
            parsingDate = true;
        else if (qName.equals("media:thumbnail") || qName.equals("media:content") || qName.equals("image")) {
            if (attributes.getValue("url") != null)
                currentItem.setImageUrl(attributes.getValue("url"));
        }
    }

    /*
        Called when an closing tag is reached, such as </item> or </title>. This should be modified
        to add or remove depending on the use it will receive (i.e. if what you want is inside
        a </title> tag, </description> tag, </description> tag, etc.)
    */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("item")) {
            // This signifies the end of an item, which adds the currentItem to the list of items.
            rssItemList.add(currentItem);
            currentItem = null;
        } else if (qName.equals("title"))
            parsingTitle = false;
        else if (qName.equals("link"))
            parsingLink = false;
        else if (qName.equals("description"))
            parsingDescription = false;
        else if (qName.equals("pubDate"))
            parsingDate = false;
    }

    /*
        Goes through the tag character by character and parses its contents.
        Example run:
                if parsingTitle is true, then that means we are inside a <title> tag,
                therefore, the string inside the tag is the title of the item
      */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (currentItem != null) {
            if (parsingTitle)
                currentItem.setTitle(new String(ch, start, length));
            else if (parsingLink)
                currentItem.setLink(new String(ch, start, length));
            else if (parsingDescription)
                currentItem.setDescription(new String(ch, start, length));
        }
    }
}

